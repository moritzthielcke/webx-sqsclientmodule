/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.sqsclient;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsyncClient;
import com.google.inject.AbstractModule;
import com.google.inject.util.Providers;
import io.vertx.core.json.JsonObject;
import io.wx.core3.config.AbstractConfigurator;
import java.util.Map;

/**
 *
 * @author moritz
 */
public class SQSConfigurator extends AbstractConfigurator {
    private SQSConfig cfg;
    
    
    public SQSConfigurator(){
        this(new SQSConfig());
    }
    
    public SQSConfigurator(SQSConfig cfg){
        this.cfg = cfg;
    }

    @Override
    public void doConfig(Map modules) {
        //check addon creds
        JsonObject creds = getAppConfig().getAddonCredentials();
        String SQSREGION = creds.getString("SQSREGION");
        String SQSACCESSKEYID = creds.getString("SQSACCESSKEYID");
        String SQSSECRETKEY = creds.getString("SQSSECRETKEY");
        
        //systemv env fall backs
        if(SQSREGION == null){
            SQSREGION = System.getenv("SQSREGION");
        }
        if(SQSACCESSKEYID == null){
            SQSACCESSKEYID = System.getenv("SQSACCESSKEYID");
        }
        if(SQSSECRETKEY == null){
            SQSSECRETKEY = System.getenv("SQSSECRETKEY");
        }
        
        if (SQSREGION != null) {
          this.cfg.setSQSregion(Region.getRegion(Regions.fromName(SQSREGION)));
          this.cfg.setSQSCredentials(new BasicAWSCredentials(SQSACCESSKEYID, SQSSECRETKEY));
        }
        // bind sqs client
        if (this.cfg.getSQSregion() != null) {
            AmazonSQSAsyncClient amazonclient =  new   AmazonSQSAsyncClient(this.cfg.getSQSCredentials());
            amazonclient.setRegion(this.cfg.getSQSregion()); // todo implement other regions      
            modules.put("SQSClient", new AbstractModule() {
               @Override
               protected void configure() {
                  bind(AmazonSQSAsyncClient.class).toInstance(amazonclient);
               }
            });   
        }
        else{
            modules.put("SQSClient", new AbstractModule() {
               @Override
               protected void configure() {
                  bind(AmazonSQSAsyncClient.class).toProvider( Providers.of(null));
               }
           });  
        }
        final SQSConfig finalCfg = this.cfg;
          modules.put("SQSConfig", new AbstractModule() {
               @Override
               protected void configure() {
                  bind(SQSConfig.class).toInstance(finalCfg);
               }
           }); 
    }
    
    
    
}