/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.modules.sqsclient;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;

/**
 *
 * @author moritz
 */
public class SQSConfig {
    private AWSCredentials SQSCredentials;
    private Region SQSregion;

    public AWSCredentials getSQSCredentials() {
        return SQSCredentials;
    }

    public void setSQSCredentials(AWSCredentials SQSCredentials) {
        this.SQSCredentials = SQSCredentials;
    }

    public Region getSQSregion() {
        return SQSregion;
    }

    public void setSQSregion(Region SQSregion) {
        this.SQSregion = SQSregion;
    }
  
  
  
}